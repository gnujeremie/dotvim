-- Désactive l'explorateur intégré pour utiliser nvim-tree ensuite
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

require('gnujeremie')
