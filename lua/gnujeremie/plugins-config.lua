-- Configuration plugins NEOVIM
require('rose-pine').setup({
    groups = {
        border = "Love"
    },
    styles = {
        transparency = true
    }
})
vim.cmd('colorscheme rose-pine-moon')

local rp = require("rose-pine.palette");

require("toggleterm").setup({
    highlights = {
        FloatBorder = {
          guifg = rp.love
        }
    },
    direction = "float"
})

require("telescope").load_extension "file_browser"
require'colorizer'.setup()

-- TREESITTER
require'nvim-treesitter.configs'.setup {
  -- A list of parser names, or "all" (the five listed parsers should always be installed)
  ensure_installed = { "vimdoc", "css", "html", "javascript", "json", "lua", "php", "scss", "sql", "typescript", "vim", "yaml" , "markdown", "markdown_inline", "angular"},

  -- Install parsers synchronously (only applied to `ensure_installed`)
  sync_install = false,

  -- Automatically install missing parsers when entering buffer
  -- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
  auto_install = true,

  highlight = {
    enable = true,

    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    additional_vim_regex_highlighting = false,
  },
}


-- TREESITTER-CONTEXT
require'treesitter-context'.setup{
  enable = true, -- Enable this plugin (Can be enabled/disabled later via commands)
  max_lines = 0, -- How many lines the window should span. Values <= 0 mean no limit.
  min_window_height = 0, -- Minimum editor window height to enable context. Values <= 0 mean no limit.
  line_numbers = true,
  multiline_threshold = 20, -- Maximum number of lines to collapse for a single context line
  trim_scope = 'outer', -- Which context lines to discard if `max_lines` is exceeded. Choices: 'inner', 'outer'
  mode = 'cursor',  -- Line used to calculate context. Choices: 'cursor', 'topline'
  -- Separator between context and content. Should be a single character string, like '-'.
  -- When separator is set, the context will only show up when there are at least 2 lines above cursorline.
  separator = '-',
  zindex = 20, -- The Z-index of the context window
}

local lspconfig = require('lspconfig')

-- LuaSnip
local ls = require("luasnip")
ls.setup({})

--[[
   [vim.keymap.set({"i"}, "<C-K>", function() ls.expand() end, {silent = true})
   [vim.keymap.set({"i", "s"}, "<C-L>", function() ls.jump( 1) end, {silent = true})
   [vim.keymap.set({"i", "s"}, "<C-J>", function() ls.jump(-1) end, {silent = true})
   [
   [vim.keymap.set({"i", "s"}, "<C-E>", function()
   [    if ls.choice_active() then
   [        ls.change_choice(1)
   [    end
   [end, {silent = true})
   ]]
require("luasnip.loaders.from_vscode").lazy_load()

-- Configuratio de LSP ZERO
local lsp = require('lsp-zero').preset({})

-- Fix Undefined global 'vim'
lsp.nvim_workspace()

lsp.on_attach(function(client, bufnr)
  lsp.default_keymaps({buffer = bufnr})
end)

lsp.set_sign_icons({
  error = '✘',
  warn = '▲',
  hint = '⚑',
  info = '»'
})

lsp.setup()

-- LSPSAGA
local saga = require('lspsaga')
saga.setup({
    outline = {
        layout = 'float'
    }
})

saga.init_lsp_saga = {
    server_filetype_map = {
        typescript = 'typescript'
    }
}

-- SONARLINT
require('sonarlint').setup({
    server = {
        cmd = {
            'sonarlint-language-server',
            -- Ensure that sonarlint-language-server uses stdio channel
            '-stdio',
            '-analyzers',
            -- $HOME/.local/share/nvim/mason
            vim.fn.expand("$MASON/share/sonarlint-analyzers/sonarjs.jar"),
        }
    },
    filetypes = {
        'javascript',
        'typescript',
    },
    flags = {
        debounce_text_changes = 1000
    }

})

vim.cmd [[
let g:lightline = {
      \ 'colorscheme': 'powerline',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'FugitiveHead'
      \ },
      \ }
]]

-- CMP autocompletion
local cmp = require('cmp')
local cmp_action = require('lsp-zero').cmp_action()
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.workspace.didChangeWatchedFiles.dynamicRegistration = false

cmp.setup({
    preselect = 'item',
    completion = {
        completeopt = 'menu,menuone,noinsert',
        autocomplete = false
    },
    mapping = {
        ['<C-Space>'] = cmp.mapping.complete(),
        ['<Tab>'] = cmp.mapping.confirm({select = false}),
    },
    snippet = {
        expand = function(args)
            require'luasnip'.lsp_expand(args.body)
        end
    },
    sources = cmp.config.sources({
        { name = 'nvim_lsp' },
        { name = 'buffer' , option = {
                get_bufnrs = function()
                    return vim.api.nvim_list_bufs()
                end
            }
        },
        { name = 'luasnip' },
    })
})

cmp.setup.filetype('gitcommit', {
    sources = cmp.config.sources({
        { name = 'cmp_git' }, -- You can specify the `cmp_git` source if you were installed it.
    }, {
        { name = 'buffer' },
    })
})


-- buffers
--[[
   [local bufferline = require("bufferline")
   [bufferline.setup{
   [    options = {
   [        mode = "buffers",
   [        indicator = {
   [            icon = '▎', -- this should be omitted if indicator style is not 'icon'
   [            style = 'icon',
   [        },
   [        -- or you can combine these e.g.
   [        diagnostics = "nvim_lsp"
   [    }
   [}
   ]]

-- oil.vim
require("oil").setup{}

-- noice.nvim
--[[
   [require("noice").setup({
   [  lsp = {
   [    -- override markdown rendering so that **cmp** and other plugins use **Treesitter**
   [    override = {
   [      ["vim.lsp.util.convert_input_to_markdown_lines"] = true,
   [      ["vim.lsp.util.stylize_markdown"] = true,
   [      ["cmp.entry.get_documentation"] = true, -- requires hrsh7th/nvim-cmp
   [    },
   [  },
   [  -- you can enable a preset for easier configuration
   [  presets = {
   [    bottom_search = true, -- use a classic bottom cmdline for search
   [    command_palette = true, -- position the cmdline and popupmenu together
   [    long_message_to_split = true, -- long messages will be sent to a split
   [    inc_rename = false, -- enables an input dialog for inc-rename.nvim
   [    lsp_doc_border = false, -- add a border to hover docs and signature help
   [  },
   [})
   ]]

-- require("dap-vscode-js").setup({
--   -- node_path = "node", -- Path of node executable. Defaults to $NODE_PATH, and then "node"
--   debugger_path = "/Users/jeremie/.config/nvim/plugged/vscode-js-debug", -- Path to vscode-js-debug installation.
--   -- debugger_cmd = { "js-debug-adapter" }, -- Command to use to launch the debug server. Takes precedence over `node_path` and `debugger_path`.
--   adapters = { 'pwa-node' }, -- which adapters to register in nvim-dap
--   -- log_file_path = "(stdpath cache)/dap_vscode_js.log" -- Path for file logging
--   -- log_file_level = false -- Logging level for output to file. Set to false to disable file logging.
--   -- log_console_level = vim.log.levels.ERROR -- Logging level for output to console. Set to false to disable console output.
-- })
--
-- --[[
--    [for _, language in ipairs({ "typescript", "javascript" }) do
--    [  require("dap").configurations[language] = {
--    [      {
--    [          type = "pwa-node",
--    [          request = "attach",
--    [          name = "Attach",
--    [          processId = function() return require'dap.utils'.pick_process { filter = "node" } end,
--    [          cwd = "${workspaceFolder}",
--    [      }
--    [  }
--    [end
--    ]]
--
-- local dap, dapui =require("dap"),require("dapui")
-- dap.listeners.after.event_initialized["dapui_config"]=function()
--   dapui.open()
-- end
-- dap.listeners.before.event_terminated["dapui_config"]=function()
--   dapui.close()
-- end
-- dap.listeners.before.event_exited["dapui_config"]=function()
--   dapui.close()
-- end
-- dapui.setup()
--
-- vim.api.nvim_create_user_command('DebugNodeJS', require('gnujeremie.dap').attach_to_remote_debugger_nodejs, {})


-- require('dressing').setup({})

-- local M = {}
--
-- function M.attach_to_remote_debugger_nodejs()
--     vim.ui.input({
--         prompt = "Enter Node Debug Port : ",
--         default = "9229",
--     }, function(input)
--         local port = tonumber(input)
--
--         require("dap").run({
--             type = "pwa-node",
--             request = "attach",
--             name = "Debug (Attach) - Remote",
--             hostName = "127.0.0.1",
--             port = port,
--         })
--     end)
--
-- end
--
-- return M

-- Aerial (toolbar avec fonctions/classes du buffer)
require('aerial').setup({
    filter_kind = false,
    -- optionally use on_attach to set keymaps when aerial has attached to a buffer
    on_attach = function(bufnr)
        -- Jump forwards/backwards with '{' and '}'
        vim.keymap.set('n', '{', '<cmd>AerialPrev<CR>', {buffer = bufnr})
        vim.keymap.set('n', '}', '<cmd>AerialNext<CR>', {buffer = bufnr})
    end
})
-- Affichage dans Téléscope
require("telescope").load_extension("aerial")
