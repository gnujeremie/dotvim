local Plug = vim.fn['plug#']

vim.call('plug#begin', '~/.config/nvim/plugged')

-- Coloration syntaxique
Plug('rose-pine/neovim', {as = 'rose-pine'}) -- coloration syntaxique
Plug('folke/tokyonight.nvim', { branch = 'main' })
Plug 'tiagovla/tokyodark.nvim'

Plug 'https://github.com/docunext/closetag.vim.git' -- fermeture automatique des balises html
Plug 'https://github.com/scrooloose/nerdcommenter.git' -- commentaires
Plug 'https://github.com/fholgado/minibufexpl.vim.git' -- toolbar avec buffers
-- Plug ('akinsho/bufferline.nvim', { ['tag'] = '*' } ) -- toolbar avec buffers
Plug 'https://github.com/can3p/incbool.vim.git' -- (in|dé)crémentation boolean
Plug 'https://github.com/airblade/vim-gitgutter.git' -- infos git dans menu de gauche
Plug 'https://github.com/mattn/emmet-vim.git' -- génération rapide de balises html (ul>li*)
Plug 'https://github.com/tpope/vim-fugitive.git' -- intégration de git
Plug 'https://github.com/tpope/vim-surround.git' -- bascule entre parenthèses/apostrophes/guillemets...
Plug 'https://github.com/tpope/vim-projectionist.git' -- Bacule entre fichier complémentaire
Plug 'junegunn/gv.vim' -- git tree viewer
Plug 'tpope/vim-unimpaired' -- ]e / [e
Plug 'https://github.com/powerman/vim-plugin-AnsiEsc.git' -- Transforme les fichiers avec de l'ansi (ex: logs pm2)
Plug 'https://github.com/tmhedberg/matchit' -- utilisation du '%' pour basculer entre 2 caractères qui matchent (if/else, ...)
Plug('nvim-telescope/telescope.nvim', { branch = '0.1.x' }) -- recherche de fichier
Plug 'stevearc/oil.nvim' -- gestionnaire de fichiers
Plug "kkharji/sqlite.lua" -- sqlite pour telescope (fichiers récemment utilisés)
-- Plug 'itchyny/lightline.vim' -- statusline
Plug 'nvim-lualine/lualine.nvim' -- statusline
Plug 'norcalli/nvim-colorizer.lua' -- coloration hexa
Plug 'nvim-neotest/nvim-nio' -- traitements asynchrones
Plug ('earthly/earthly.vim', { ['branch'] = 'main' })
Plug('stevearc/aerial.nvim') -- menu des classes/fonctions/méthodes
Plug('iamcco/markdown-preview.nvim', { ['do'] = 'cd app && npx --yes yarn install' })

-- Plug 'mbbill/undotree' -- Arbre des modifications du buffer
-- Plug('theprimeagen/harpoon') -- fichiers en marquepages
-- Plug 'vuciv/vim-bujo' -- gestion des todo
-- Plug 'puremourning/vimspector' -- debugger
-- Plug 'mfussenegger/nvim-dap' -- debugger
-- Plug 'mxsdev/nvim-dap-vscode-js' -- debug
-- Plug('microsoft/vscode-js-debug', { ['do'] = 'npm install --legacy-peer-deps && npx gulp vsDebugServerBundle && mv dist out' }) -- debug
-- Plug 'rcarriga/nvim-dap-ui' -- ui du debug
-- Plug ('danymat/neogen')
-- Plug 'nvim-treesitter/nvim-tree-docs'

--- Treesitter
Plug('nvim-treesitter/nvim-treesitter', {['do'] = ':TSUpdate'}) -- Aide au developpement (analyse de code)
Plug 'nvim-treesitter/nvim-treesitter-context' -- affichage du context du curseur en haut de la fenêtre
Plug 'nvim-treesitter/playground' -- affichage de l'analyse qui a été faite du fichier par treesitter
Plug 'nvim-telescope/telescope-file-browser.nvim' -- explorateur de fichier pour telescope
Plug 'nvim-tree/nvim-web-devicons' -- icônes
Plug 'othree/yajs.vim' -- syntax JS (Treesitter)
Plug 'othree/html5.vim' -- syntax html (Treesitter)
--Plug 'https://github.com/HerringtonDarkholme/yats.vim/' -- syntax TS (Treesitter)

-- LSP
Plug('VonHeikemen/lsp-zero.nvim', {branch = 'v2.x'}) -- communication via LSP
Plug 'neovim/nvim-lspconfig' -- communication via LSP
Plug('williamboman/mason.nvim', {['do'] = ':MasonUpdate'}) -- manager des plugins d'analyse de fichier/syntaxe...
Plug 'williamboman/mason-lspconfig.nvim' -- manager des plugins d'analyse de fichier/syntaxe...
Plug 'https://gitlab.com/schrieveslaach/sonarlint.nvim.git' -- plugin pour sonarlint' -- plugin pour sonarlint
Plug 'hrsh7th/nvim-cmp' -- moteur de complétion
Plug 'hrsh7th/cmp-nvim-lsp' -- moteur de complétion
Plug 'hrsh7th/cmp-buffer' -- complétion
Plug 'hrsh7th/cmp-path' -- complétion
Plug 'hrsh7th/cmp-cmdline' -- complétion
Plug('saadparwaiz1/cmp_luasnip') -- luasnip pour cmp
Plug('L3MON4D3/LuaSnip', { tag = 'v2.*', ['do'] = 'make install_jsregexp' }) -- snippets
Plug('rafamadriz/friendly-snippets') -- snippets
Plug 'nvim-lua/plenary.nvim' -- lua fonctions utilisées par d'autres plugins
Plug('nvimdev/lspsaga.nvim') -- affichage des erreurs/lints (optim la config de lsp)
-- Plug 'stevearc/dressing.nvim' -- mise en forme des UI/telescope

--[[
   [Plug 'folke/noice.nvim' -- remplace la console
   [Plug 'MunifTanjim/nui.nvim' -- dépendance de noice.nvim
   ]]

Plug 'akinsho/toggleterm.nvim'

vim.call('plug#end')
