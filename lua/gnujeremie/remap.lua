-- Mappings

local keymap = vim.keymap.set

keymap("i", "jj", "<Esc>")
keymap("i", "<c-j>", "<Esc>Ji")
keymap("n", "<leader>l", ":let @/ = \"\"<CR>")
keymap("n", "<space><space>", ":w<cr>")
keymap("t", "jj", "<C-\\><C-n>") -- Permet de quitter le mode "terminal"

keymap("v", "J", ":m '>+1<CR>gv=gv")
keymap("v", "K", ":m '<-2<CR>gv=gv")

keymap("n", "<leader><leader>", function()
    vim.cmd("so")
end)

keymap("n", "<m-h>", "<C-w>h")
keymap("n", "<m-j>", "<C-w>j")
keymap("n", "<m-k>", "<C-w>k")
keymap("n", "<m-l>", "<C-w>l")

keymap("x", "ga", "<Plug>(EasyAlign)")
keymap("n", "ga", "<Plug>(EasyAlign)")

keymap("i", "['", "['']<ESC>hi")
keymap("i", "{'", "{''}<ESC>hi")
keymap("i", "('", "('')<ESC>hi")
keymap("i", "gfi",  "fix()<ESC>ha")
keymap("i", "gup",  "update()<ESC>ha")
keymap("i", "gde",  "delete()<ESC>ha")
keymap("i", "gad",  "add()<ESC>ha")
keymap("i", "gcl",  "update(changelog)<ESC>")
keymap("i", "trc",  "try {<ESC>o} catch (e) {<ESC>o}<ESC>2ko")
keymap("i", "ifl",  "if () {<ESC>o} else {<ESC>o}<ESC>2ko")

-- pm2
keymap('n', '<leader>pml', ':!pm2 reload all<CR>')
keymap('n', '<leader>pmp', ':!pm2 reload app-pulse<CR>')
keymap('n', '<leader>pma', ':!pm2 reload app-admin<CR>')
keymap('n', '<leader>pme', ':!pm2 reload app-eventing<CR>')
keymap('n', '<leader>pmq', ':!pm2 reload app-queue<CR>')
keymap('n', '<leader>pmo', ':!pm2 reload app-oauth<CR>')

-- telescope file browser
keymap("n", "<c-n>", ":Telescope file_browser<CR>") -- Affichage de l'explorateur
keymap("n", "<c-p>", ":Telescope file_browser path=%:p:h select_buffer=true<CR>") -- ouvre l'exploration sur le fichier en cours

-- Minibufferexplorer
-- showtabline=0/2
keymap("n", "<m-b>", ":MBEToggle<CR>")
keymap("n", "<m-p>", ":bp<CR>")
keymap("n", "<m-n>", ":bn<CR>")

-- Fugitive
keymap("n", "<c-g>s", ":Git<CR>")
keymap("n", "<c-g>po", ":Git push origin<CR>")
keymap("n", "<c-g>pu", ":Git pull origin<CR>")
keymap("n", "<c-g>fo", ":Git fetch origin<CR>")
keymap("n", "<c-g>pg", ":Git push github<CR>")

-- Gitv
--[[
   [keymap("n", "<c-g>l", ":Gitv<CR>")
   [keymap("n", "<c-g>ll", ":Gitv!<CR>")
   ]]

-- Telescope
keymap("n", "<leader>ff", "<cmd>Telescope find_files<cr>")
keymap("n", "<leader>fg", "<cmd>Telescope live_grep<cr>")
keymap("n", "<leader>fb", "<cmd>Telescope buffers<cr>")
keymap('n', '<leader>ft', '<cmd>Telescope aerial<CR>')
-- keymap("n", "<leader>fh", "<cmd>Telescope help_tags<cr>")
-- keymap("n", "<leader>fd", require("telescope.builtin").diagnostics)

-- Lspsaga
-- Renomme une variable
keymap("n", "<leader>lr", "<cmd>Lspsaga rename<CR>")
-- Ouvre un terminal
keymap("n", "<leader>t", "<cmd>Lspsaga term_toggle<CR>")

-- Affichage preview définition
-- keymap("n", "gh", "<cmd>Lspsaga lsp_finder<CR>")

-- Diagnostics du buffer
keymap("n", "<leader>fd", "<cmd>Lspsaga show_buf_diagnostics<CR>")

-- Diagnostic de la ligne
keymap("n", "<leader>sl", "<cmd>Lspsaga show_line_diagnostics<CR>")

-- Code action
keymap({"n","v"}, "<leader>fa", "<cmd>Lspsaga code_action<CR>")

-- peek definition
keymap("n", "gp", "<cmd>Lspsaga peek_definition<CR>")

-- Navigation dans les erreurs
keymap("n", "[e", "<cmd>Lspsaga diagnostic_jump_prev<CR>")
keymap("n", "]e", "<cmd>Lspsaga diagnostic_jump_next<CR>")

keymap("n", "K", "<cmd>Lspsaga hover_doc<CR>")
-------

-- undotree
keymap('n', '<leader>u', "<cmd>UndotreeToggle<CR>")

-- Aerial
keymap('n', '<C-t>', '<cmd>AerialToggle!<CR>')

-- unimpaired
keymap('n', '<leader>[e', '<Plug>(unimpaired-move-up)')
keymap('n', '<leader>]e', '<Plug>(unimpaired-move-down)')

-- ToggleTerm
keymap('n', '<leader>t', '<cmd>ToggleTerm<CR>')

-- nvim-dap
-- keymap('n', '<Leader>dr', ":DebugNodeJS<CR>")
-- keymap('n', '<Leader>dd', require 'dap'.continue)
-- keymap('n', '<Leader>dn', require 'dap'.step_over)
-- keymap('n', '<Leader>di', require 'dap'.step_into)
-- keymap('n', '<Leader>do', require 'dap'.step_out)
-- keymap('n', '<leader>db', require 'dap'.toggle_breakpoint)
-- keymap('n', '<Leader>dc', require 'dapui'.close)
