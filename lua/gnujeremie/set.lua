-- Configuration
local autocmd = vim.api.nvim_create_autocmd

-- JAVA_HOME perso
vim.env.JAVA_HOME="/Library/Java/JavaVirtualMachines/jdk-20.0.1.jdk/Contents/Home"

vim.g.mapleader=","
vim.opt.completeopt = "menu"

-- vim.g.disable_netrw = true
-- vim.g.hijack_netrw = true
-- vim.g.hijack_directories = true

vim.opt.termguicolors = true

vim.opt.backup = false
vim.opt.swapfile = false
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

vim.opt.nu = true
vim.opt.textwidth = 120
vim.opt.showcmd = true
vim.opt.laststatus = 2
vim.opt.cursorline = true
-- allow backspace everywhere in insert mode
vim.opt.backspace = "indent,eol,start"
-- hide buffers when not displayed
vim.opt.hidden = true

vim.opt.expandtab = true
vim.opt.shiftwidth = 4
vim.opt.softtabstop = 4
vim.opt.autoindent = true
vim.opt.smartindent = true
vim.opt.list = true
vim.opt.listchars = "tab:▷⋅,trail:⋅,nbsp:⋅"

--vim.cmd('colorscheme tokyonight-night')
-- Rose moon activé dans plugins-config.lua

autocmd('BufWritePre', {
  pattern = '',
  command = ":%s/\\s\\+$//e"
})

-- Minibuf
vim.g.miniBufExplMapWindowNavVim = "0"
vim.g.miniBufExplMapCTabSwitchBufs = "1"

-- Gitv
vim.g.Gitv_OpenHorizontal = "0"
vim.opt.lazyredraw = true

-- Gitgutter
vim.g.gitgutter_realtime = "0"
vim.g.gitgutter_eager = "0"

-- lsp-zero
-- You will likely want to reduce updatetime which affects CursorHold
-- note: this setting is global and should be set only once
vim.o.updatetime = 250
-- vim.cmd [[autocmd! CursorHold,CursorHoldI * lua vim.diagnostic.open_float(nil, {focus=false})]]

vim.g['bujo#window_width'] = 80

-- nvim-dap
vim.fn.sign_define('DapBreakpoint',{ text ='🟥', texthl ='', linehl ='', numhl =''})
vim.fn.sign_define('DapStopped',{ text ='▶️', texthl ='', linehl ='', numhl =''})

-- Folding
-- zf#j creates a fold from the cursor down # lines.
-- zf/string creates a fold from the cursor to string .
-- zj moves the cursor to the next fold.
-- zk moves the cursor to the previous fold.
-- zo opens a fold at the cursor.
-- zO opens all folds at the cursor.
-- zm increases the foldlevel by one.
-- zM closes all open folds.
-- zr decreases the foldlevel by one.
-- zR decreases the foldlevel to zero -- all folds will be open.
-- zd deletes the fold at the cursor.
-- zE deletes all folds.
-- [z move to start of open fold.
-- ]z move to end of open fold.
-- uses treesitter
vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "v:lua.vim.treesitter.foldexpr()"
vim.opt.foldtext = ""
vim.opt.foldlevel = 99
vim.opt.foldlevelstart = 1
vim.opt.foldnestmax = 4 -- Limit depth
