-- Génération d'un UUID et insertion là où est le cursor
--
-- Fonction pour exécuter un script JavaScript
local function execute_js_script(script)
  -- Utilise io.popen pour exécuter le script avec Node.js et capturer la sortie
  local handle = io.popen("node -e '" .. script .. "'")
  local result = handle:read("*a")  -- Lire tout le retour
  handle:close()  -- Fermer le handle

  return result
end

-- Fonction pour insérer du texte à l'endroit du curseur
local function insert_at_cursor(text)
  -- Obtenir la position actuelle du curseur (ligne, colonne)
  local row, col = unpack(vim.api.nvim_win_get_cursor(0))
  text = text:match("[^\n]*")
  vim.api.nvim_buf_set_text(0, row - 1, col, row - 1, col, { text })
end

-- Exemple : une fonction qui exécute un script JS
local function generate_uuid()
  -- Script JavaScript à exécuter
  local script = [[
        const uuid = require("crypto").randomUUID
        console.log(uuid().replace(/-/g, ""));
        process.exit(0);
  ]]

  -- Exécuter le script et récupérer le résultat
  local output = execute_js_script(script)

  -- Afficher le résultat dans Neovim
  -- print("JavaScript output: " .. output)
  insert_at_cursor(output)
end

-- Créer une commande Neovim pour appeler cette fonction
vim.api.nvim_create_user_command('GenerateUUID', generate_uuid, {})
vim.keymap.set('i', 'guid', generate_uuid, { noremap = true, silent = true })
